const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config.js');

let app = express();

const expressWs = require('express-ws')(app);

const jwtExpress = require('express-jwt');

const jwt = require('jsonwebtoken');

const cards_data = require('./data/cards.js');


//--------------------------------------------------
// to support JSON-encodede bodies
app.use(bodyParser.json());

// to support URL-encoded bodies
app.use(bodyParser.urlencoded({extended: true}));

//--------------------------------------------------
// cors options
app.use(cors(config.corsOptions));

//--------------------------------------------------

// game
let gameData = {
  playerIds: 1,
  players: [],

  roomIds: 1,
  rooms: [],

  allSockets: []
}

function generateRandomCardsForPlayers(room) {
  let cardsDeck = gameData.rooms[room.name + '-' + room.id].cards;
  let roomPlayers = gameData.rooms[room.name + '-' + room.id].players;

  let generatedCards = [];
  let howManyCards = 7;

  Object.keys(roomPlayers).map((item, i) => {
    let userEntry = roomPlayers[item];

    for(var cardIndex = 0; cardIndex < (howManyCards); cardIndex++) {
      let cardsDeckLength = cardsDeck.length;
      let randomCardIndex = parseInt(Math.random() * (cardsDeckLength - 0) + 0);

      //add card for the player
      userEntry.cards.push(cardsDeck[randomCardIndex]);

      //remove the card from game cards deck
      cardsDeck.splice(randomCardIndex, 1);
    }

    //update player in room array
    roomPlayers[item] = userEntry;

  });

  //update players and cards deck in room array
  gameData.rooms[room.name + '-' + room.id].players = roomPlayers;
  gameData.rooms[room.name + '-' + room.id].cards = cardsDeck;

  return true;
}

function generateFirstCard(room) {
  let cardsDeck = gameData.rooms[room.name + '-' + room.id].cards;
  let cardsDeckLength = cardsDeck.length;
  let randomCardIndex = parseInt(Math.random() * (cardsDeckLength - 0) + 0);

  //set the first card
  gameData.rooms[room.name + '-' + room.id].gameLastCard = cardsDeck[randomCardIndex];

  //if its a black card
  if(cardsDeck[randomCardIndex][0] === 'black') {
    gameData.rooms[room.name + '-' + room.id].gamePickedColor = 'red';
  }

  //remove this card from whole deck
  cardsDeck.splice(randomCardIndex, 1);

  //update the room array
  gameData.rooms[room.name + '-' + room.id].cards = cardsDeck;

  return true
}

function pickStartingPlayer(room) {
  let roomPlayers = gameData.rooms[room.name + '-' + room.id].players;
  let playersCount = Object.keys(roomPlayers).length;
  let randomPlayerIndex = parseInt(Math.random() * (playersCount - 0) + 0);
  let playerEntry;

  Object.keys(roomPlayers).map((item, i) => {
    if(i === randomPlayerIndex) {
      playerEntry = gameData.rooms[room.name + '-' + room.id].players[item];
    }
  });

  //set current player
  gameData.rooms[room.name + '-' + room.id].gameCurrentPlayer = playerEntry.nickname+'-'+playerEntry.id;

  return true;
}

function setGameState(state, room) {
  if(state) {
    //start game
    generateRandomCardsForPlayers(room);
    generateFirstCard(room);
    pickStartingPlayer(room);

    gameData.rooms[room.name + '-' + room.id].gameWinner = false;

    console.log('Game started. Cards generated!');

    return true;
  }else {
    //stop game
    let roomPlayers = gameData.rooms[room.name + '-' + room.id].players;
    let fullCardsDeck = cards_data.availableCards;

    Object.keys(roomPlayers).map((item, i) => {
      gameData.rooms[room.name + '-' + room.id].players[item].cards = [];
    });

    gameData.rooms[room.name + '-' + room.id].gameLastCard = false;
    gameData.rooms[room.name + '-' + room.id].gameDirection = 'ltr';
    gameData.rooms[room.name + '-' + room.id].gameCurrentPlayer = 0;
    gameData.rooms[room.name + '-' + room.id].gamePickedColor = false;
    gameData.rooms[room.name + '-' + room.id].cards = [...fullCardsDeck];

    console.log('Game stopped!');

    return true;
  }

  gameData.rooms[room.name + '-' + room.id].gameState = state;
}

function checkAndStoreWebsocket(ws, req) {
  let token = req.headers['sec-websocket-protocol'];

  // validate token
  jwt.verify(token, config.jsonwebtokenConfig.jwtSecret, function(err, user) {
    if (!err){
      console.log('New player connected!');

      //player has a valid token, but is not in array
      if(!gameData.players[user.nickname + '-' + user.id]) {
        gameData.players[user.nickname + '-' + user.id] = {
          id: user.id,
          nickname: user.nickname,
          room: user.room,
          roomName: user.roomName
        };

        console.log('and has already a token and not an array!');
      }

      gameData.allSockets.push({user_id: user.id, user_nickname: user.nickname, socket: ws});
    }
  });
}

app.ws('/api/socks', (ws, req) => {

  checkAndStoreWebsocket(ws, req);

  // remove ws from array onClose
  ws.on('close', () => {
    gameData.allSockets = gameData.allSockets.filter(row => {
      if(row.socket === ws) {
        let userId = row.user_id;
        let userNickname = row.user_nickname;
        if(gameData.players[userNickname + '-' + userId]) {
          delete gameData.players[userNickname + '-' + userId];

          //remove user from room
          Object.keys(gameData.rooms).map((item, i) => {
            if(gameData.rooms[item].players[userNickname+'-'+userId]) {
              delete gameData.rooms[item].players[userNickname+'-'+userId];

              //send other player the info
              Object.keys(gameData.players).map((playerItem, playerIndex) => {
                let userEntry = gameData.players[playerItem];

                let userSocket = gameData.allSockets.find(function(element) {
                  return element.user_id === userEntry.id;
                });

                if (userSocket && userEntry.room === gameData.rooms[item].id && userEntry.roomName === gameData.rooms[item].name) userSocket.socket.send('*');

              });
            }
          });


          console.log('Player left the game. Removing his array!');
        }
      }

      return (row.socket === ws) ? false : true;
    });
  });
});

// Check token function
app.post('/api/checkToken', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let payload = jwt.decode(token);

  if(token && payload) {

    //player has a valid token, but is not in array
    if(!gameData.players[payload.nickname + '-' + payload.id]) {
      gameData.players[payload.nickname + '-' + payload.id] = {
        id: payload.id,
        nickname: payload.nickname,
        room: payload.room,
        roomName: payload.roomName
      };
    }

    let newPayload = gameData.players[payload.nickname + '-' + payload.id];

    const token = jwt.sign(newPayload, config.jsonwebtokenConfig.jwtSecret, config.jsonwebtokenConfig.jwtConfig);

    res.send({user: newPayload, token:token});
  }else {
    res.status(500).send('No token found!');
  }

});

//login function
app.post('/api/login', function(req, res) {
  let user = req.body.user;

  if(user) {
    let userId = gameData.playerIds+1;

    const payload = {
      id: userId,
      nickname: user.nickname,
      room: 0,
      roomName: 0
    };

    // gen token
    const token = jwt.sign(payload, config.jsonwebtokenConfig.jwtSecret, config.jsonwebtokenConfig.jwtConfig);

    // push player into array
    gameData.players[payload.nickname + '-' + payload.id] = {
      id: payload.id,
      nickname: payload.nickname,
      room: 0,
      roomName: 0
    };

    gameData.playerIds = userId;

    // Send user entry and token
    res.send({user:payload, token:token});
  } else {
    // No user entry in post request
    res.sendStatus(500);
  }

});


// get rooms data
app.post('/api/getRoomsData', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let payload = jwt.decode(token);

  if(token && payload) {

    if(gameData.players[payload.nickname + '-' + payload.id]) {

      //remove user from all rooms
      Object.keys(gameData.rooms).map((item, i) => {
        let roomEntry = gameData.rooms[item];

        if(roomEntry.players[payload.nickname + '-' + payload.id]) {
          console.log('removed player from room id: '+roomEntry.id);
          delete gameData.rooms[roomEntry.name + '-' + roomEntry.id].players[payload.nickname + '-' + payload.id];

          gameData.players[payload.nickname + '-' + payload.id].room = 0;
          gameData.players[payload.nickname + '-' + payload.id].roomName = 0;
        }
      });

      res.send({ user: payload, rooms: {...gameData.rooms} });
    }else {
      res.status(500).send('No player found!');
    }

  }else {
    res.status(500).send('No token found!');
  }

});

app.post('/api/createRoom', function(req, res) {
  let user = req.body.user;
  let room = req.body.room;

  let roomId = gameData.roomIds+1;

  let cardsDeck = cards_data.availableCards;

  gameData.rooms[room.name + '-' + roomId] = {
    id: roomId,
    name: room.name,
    owner: {name: user.nickname, id: user.id},
    type: room.type,
    password: room.password,
    createDate: Date.now(),
    gameState: false,
    gameDirection: 'ltr',
    gameCurrentPlayer: 0,
    gameLastCard: false,
    gamePickedColor: false,
    gameWinner: false,
    players: [],
    chat: [],
    cards: [...cardsDeck]
  };

  gameData.roomIds = roomId;

  //send other player the info
  Object.keys(gameData.players).map((item, i) => {
    let userEntry = gameData.players[item];

    let userSocket = gameData.allSockets.find(function(element) {
      return element.user_id === userEntry.id;
    });

    if (userSocket && userEntry.room === 0) userSocket.socket.send('*');

  });

  res.send();

});

app.post('/api/getRoomById', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let user = jwt.decode(token);
  let roomId = req.body.roomId;

  if(token && user) {

    if(gameData.players[user.nickname + '-' + user.id]) {
      if(roomId) {

        Object.keys(gameData.rooms).map((item, i) => {
          let roomEntry = gameData.rooms[item];

          if(parseInt(roomEntry.id) === parseInt(roomId)) {

            //push player into room array if he's not there
            if(!roomEntry.players[user.nickname + '-' + user.id]) {
              gameData.rooms[roomEntry.name + '-' + roomEntry.id].players[user.nickname + '-' + user.id] = {
                id: user.id,
                nickname: user.nickname,
                status: 0,
                cards: []
              }
            }

            //update user
            gameData.players[user.nickname + '-' + user.id].room = roomEntry.id;
            gameData.players[user.nickname + '-' + user.id].roomName = roomEntry.name;

            //get players from room
            let playersInRoom = gameData.rooms[roomEntry.name + '-' + roomEntry.id].players;

            //update token
            const token = jwt.sign(gameData.players[user.nickname + '-' + user.id], config.jsonwebtokenConfig.jwtSecret, config.jsonwebtokenConfig.jwtConfig);

            res.send({ room: gameData.rooms[roomEntry.name + '-' + roomEntry.id], players: { ...playersInRoom}, token: token });
          }

        });

      }else {
        res.status(500).send('No room id given!');
      }
    }else {
      res.status(500).send('Player is not in array!');
    }
  }else {
    res.status(500).send('No player token found!');
  }

});

app.post('/api/connectHandler', function(req, res) {
  let roomId = req.body.id;

  //send info for other players in room
  Object.keys(gameData.players).map((item, i) => {
    let userEntry = gameData.players[item];

    let userSocket = gameData.allSockets.find(function(element) {
      return element.user_id === userEntry.id;
    });

    if (userSocket && userEntry.room === parseInt(roomId)) userSocket.socket.send('*');
  });

  res.send();
});

app.post('/api/leaveHandler', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let payload = jwt.decode(token);

  if(token && payload) {

    if(gameData.players[payload.nickname + '-' + payload.id]) {

      //remove user from all rooms
      Object.keys(gameData.rooms).map((item, i) => {
        let roomEntry = gameData.rooms[item];

        if(roomEntry.players[payload.nickname + '-' + payload.id]) {
          console.log('player disconected from room id: '+roomEntry.id);
          delete gameData.rooms[roomEntry.name + '-' + roomEntry.id].players[payload.nickname + '-' + payload.id];

          gameData.players[payload.nickname + '-' + payload.id].room = 0;
          gameData.players[payload.nickname + '-' + payload.id].roomName = 0;

          //send info for other players in room
          Object.keys(gameData.players).map((item, i) => {
            let userEntry = gameData.players[item];

            let userSocket = gameData.allSockets.find(function(element) {
              return element.user_id === userEntry.id;
            });

            if (userSocket && userEntry.room === roomEntry.id) userSocket.socket.send('*');
          });

          //update token
          const token = jwt.sign(gameData.players[payload.nickname + '-' + payload.id], config.jsonwebtokenConfig.jwtSecret, config.jsonwebtokenConfig.jwtConfig);

          res.send({ user: gameData.players[payload.nickname + '-' + payload.id], token: token });
        }
      });

    }

  }
});

app.post('/api/changePlayerState', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let user = jwt.decode(token);

  let newState = req.body.newState;

  if(token && user) {

    if(gameData.players[user.nickname + '-' + user.id]) {
      let userEntry = gameData.players[user.nickname + '-' + user.id];
      let roomId = userEntry.room;
      let roomName = userEntry.roomName;

      //update state in rooms array
      gameData.rooms[roomName + '-' + roomId].players[user.nickname + '-' + user.id].status = newState;

      //check if all users are redy
      let userCount = Object.keys(gameData.rooms[roomName + '-' + roomId].players).length;
      let readyUsersCount = 0;
      Object.keys(gameData.rooms[roomName + '-' + roomId].players).map((item, i) => {
        let userEntry = gameData.rooms[roomName + '-' + roomId].players[item];

        if(userEntry.status === 1) {
          readyUsersCount++;
        }

      });

      if(userCount === readyUsersCount) {
        //game is ready to start
        setGameState(true, gameData.rooms[roomName + '-' + roomId]);
      }else {
        //game is not ready to start
        setGameState(false, gameData.rooms[roomName + '-' + roomId]);
      }

      //send info for other players in room
      Object.keys(gameData.players).map((item, i) => {
        let userEntry = gameData.players[item];

        let userSocket = gameData.allSockets.find(function(element) {
          return element.user_id === userEntry.id;
        });

        if (userSocket && userEntry.room === roomId) userSocket.socket.send('*');

      });

      res.send();
    }else {
      res.status(500).send('No player array found!');
    }
  }else {
    res.status(500).send('No player token found!');
  }
});

function setNextPlayer(room, currentUser, direction_state) {
  let roomPlayers = gameData.rooms[room.name + '-' + room.id].players;
  let roomDirection = gameData.rooms[room.name + '-' + room.id].gameDirection;
  let playersEntry = Object.keys(roomPlayers);
  let currentPlayerIndex;
  let newPlayerIndex;

  for(var i = 0; i < playersEntry.length; i++) {
    if(playersEntry[i] === currentUser) {
      currentPlayerIndex = i;
    }
  }

  if(direction_state === 'new_direction') {
    roomDirection = (room.gameDirection === 'ltr') ? 'rtl' : 'ltr';
    gameData.rooms[room.name + '-' + room.id].gameDirection = roomDirection;
  }

  if(roomDirection === 'ltr') {
    if(currentPlayerIndex === playersEntry.length-1) {
      newPlayerIndex = 0;
    }else {
      newPlayerIndex = currentPlayerIndex+1;
    }
  }else {
    if(currentPlayerIndex === 0) {
      newPlayerIndex = playersEntry.length-1;
    }else {
      newPlayerIndex = currentPlayerIndex-1;
    }
  }

  console.log('Direction: '+roomDirection);

  //set current player
  gameData.rooms[room.name + '-' + room.id].gameCurrentPlayer = playersEntry[newPlayerIndex];

  return true
}

app.post('/api/placeCard', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let user = jwt.decode(token);

  let color = req.body.color;
  let card = req.body.card;
  let cardIndex = req.body.cardIndex
  let roomIndex = req.body.room
  let pickedColor = req.body.pickedColor;

  if(token && user && color && card && roomIndex && pickedColor) {

    let room = gameData.rooms[roomIndex];
    if(room) {

      let cardInTheMiddle = room.gameLastCard;
      let placeBlock = true;

      if(color === 'black') {
        placeBlock = false;
      }else if(color === cardInTheMiddle[0]) {
        placeBlock = false;
      }else if(card === cardInTheMiddle[1]) {
        placeBlock = false;
      }else if(cardInTheMiddle[0] === 'black') {
        if(color === room.gamePickedColor) {
          placeBlock = false;
        }
      }else {
        placeBlock = true;
      }

      if(!placeBlock) {
        let player = room.players[user.nickname+'-'+user.id];

        gameData.rooms[roomIndex].gameLastCard = [color, card];
        player.cards.splice(cardIndex, 1);

        //if its not a black card - reset picked color
        if(color !== 'black') {
          gameData.rooms[roomIndex].gamePickedColor = false;
        }else {
          gameData.rooms[roomIndex].gamePickedColor = pickedColor;
        }

        //set next player
        if(card === 'direction') {
          setNextPlayer(room, user.nickname+'-'+user.id, 'new_direction');
        }else if(card === 'stop') {
          setNextPlayer(room, user.nickname+'-'+user.id, 'normal_direction', 'stop_card');
        }else {
          setNextPlayer(room, user.nickname+'-'+user.id, 'normal_direction');
        }

        //check if the player is winning
        if(player.cards.length === 0) {
          gameData.rooms[roomIndex].gameWinner = [user.nickname, user.id];

          //set all players in this room "not ready" status
          Object.keys(gameData.rooms[roomIndex].players).map((item, i) => {
            let roomUserEntry = gameData.rooms[roomIndex].players[item];
            gameData.rooms[roomIndex].players[roomUserEntry.nickname + '-' + roomUserEntry.id].status = false;
          })

          //stop the game
          setGameState(false, gameData.rooms[roomIndex]);

          //after send - reset the winner
          setTimeout(() => {
            gameData.rooms[roomIndex].gameWinner = false;
            console.log('setting the winner to false');
          }, 1500)
        }

        //send info for other players in room
        Object.keys(gameData.players).map((item, i) => {
          let userEntry = gameData.players[item];

          let userSocket = gameData.allSockets.find(function(element) {
            return element.user_id === userEntry.id;
          });

          if (userSocket && userEntry.room === room.id) userSocket.socket.send('*');

        });

        res.send();

      }else {
        console.log('place block');
        res.status(500).send('System error!');
      }

    }else {
      console.log('no room');
      res.status(500).send('System error!');
    }
  }else {
    console.log('not all data');
    res.status(500).send('System error!');
  }

});

function takeRandomCard(room, user) {
  let cardsDeck = gameData.rooms[room.name + '-' + room.id].cards;
  let cardsDeckLength = cardsDeck.length;
  let randomCardIndex = parseInt(Math.random() * (cardsDeckLength - 0) + 0);

  //give the card for the player
  gameData.rooms[room.name + '-' + room.id].players[user].cards.push(cardsDeck[randomCardIndex]);

  //remove this card from whole deck
  cardsDeck.splice(randomCardIndex, 1);

  //update the room array
  gameData.rooms[room.name + '-' + room.id].cards = cardsDeck;

  return true;
}

app.post('/api/takeCard', function(req, res) {
  let token = req.headers.authorization.split(' ')[1];
  let user = jwt.decode(token);

  let roomIndex = req.body.room

  if(token && user && roomIndex) {

    let room = gameData.rooms[roomIndex];
    if(room) {

      takeRandomCard(room, user.nickname+'-'+user.id)

      //set next player
      setNextPlayer(room, user.nickname+'-'+user.id);

      //send info for other players in room
      Object.keys(gameData.players).map((item, i) => {
        let userEntry = gameData.players[item];

        let userSocket = gameData.allSockets.find(function(element) {
          return element.user_id === userEntry.id;
        });

        if (userSocket && userEntry.room === room.id) userSocket.socket.send('*');

      });

      res.send();

    }else {
      res.status(500).send('System error!');
    }
  }else {
    res.status(500).send('System error!');
  }
});

//--------------------------------------------------

app.set('port', config.expressConfig.port);

app.listen(app.get('port'), function() {
  console.log('Express server listening on port '+app.get('port'));
});
