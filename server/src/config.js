module.exports = {

  'expressConfig': {
    port: 5000
  },

  'jsonwebtokenConfig': {
    jwtSecret: '',
    jwtConfig: {
      algorithm: 'HS256',
      expiresIn: '1h',
      noTimestamp: false
    }
  },

  'corsOptions': {
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Access-Control-Allow-Headers', 'Authorization', 'X-Requested-With']
  },

};
