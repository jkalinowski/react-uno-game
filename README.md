# React UNO

>  An App build to enhance React and NodeJS knowledge. Its supposed to be like a UNO card game, but in your browser on any device.


# Tech

*  React
*  NodeJS (express)
*  Websockets


# Development

>  **NOTE:** If you want to check out the whole Application, you need to have the [Client](https://gitlab.com/jkalinowski/react-uno-game/-/tree/master/client) and the [Server](https://gitlab.com/jkalinowski/react-uno-game/-/tree/master/server)

Once node is installed, clone the repositories and run npm install

# Running the project

[Server](https://gitlab.com/jkalinowski/react-uno-game/-/tree/master/server)

```sh
node src/server.js
or use nodemon
nodemon src/server.js
```

The server will run on http://localhost:5000


[Client](https://gitlab.com/jkalinowski/react-uno-game/-/tree/master/client)

```sh
npm run start
# or shorthand alias
npm start
```

open http://localhost:3000 in your browser
