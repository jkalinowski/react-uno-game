import React, {Component} from 'react'
import {Redirect} from 'react-router-dom';

import {Container, Form, Button, Dimmer, Loader} from 'semantic-ui-react';

import apiService from '../../service/apiService';

import auth from '../../service/auth';

import {session} from '../../service/Session';


class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nickname: '',
      redirectToReferrer: 'loading'
    };

    this.changeHandler = this.changeHandler.bind(this);

    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const token = session.get('token');
    apiService.checkToken(token).then(data => {
      auth.authenticate(data.user, data.token,() => {
        this.setState({ redirectToReferrer: 'loaded' });
      });
    }).catch(error => {
      this.setState({ redirectToReferrer: 'loading-failed' });
    });
  }

  changeHandler(evt, {name, value}) {
    this.setState({nickname: value});
  }

  submit() {
    let nickname = this.state.nickname;
    if(nickname !== '') {
      let user = {
        nickname: nickname,
      };

      apiService.login(user).then(data => {
        auth.authenticate(data.user, data.token,() => {
          this.setState({ redirectToReferrer: 'loaded' });
        });
      }).catch(error => {
        this.setState({ redirectToReferrer: 'loading-failed' });
      });
    }
  }

  render() {

    switch (this.state.redirectToReferrer) {
      case 'loading':
        return (
          <Dimmer active>
            <Loader />
          </Dimmer>
        );
      case 'loaded':
        const { from } = this.props.location.state || { from: { pathname: '/rooms' } };

        return <Redirect to={from} />;
      case 'loading-failed':
        return (
          <Container>
            <Form onSubmit={this.submit}>
              <h4>Enter your nickname</h4>
              <Form.Field>
                <Form.Input fluid name='nickname' icon='user' iconPosition='left' placeholder='Your nickname' onChange={this.changeHandler} />
              </Form.Field>
              <Button type='submit' color='blue' fluid size='large'>Submit</Button>
            </Form>
          </Container>
        );
      default:
        return (
          <Dimmer active>
            <Loader />
          </Dimmer>
        );
    }
  }

}

export default Login;
