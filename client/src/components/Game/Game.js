import React, {Component} from 'react';

import {Grid, Button, Header, Segment, List, Label} from 'semantic-ui-react';

import './Game.css';

import {session} from '../../service/Session';
import auth from '../../service/auth';

import apiService from '../../service/apiService';

import Player from './Player';
import PickColorModal from './PickColorModal';
import WinnerModal from './WinnerModal';

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      roomLeaveRedirect: false,
      dirty: false,
      colorPickModalState: false,
      selectedColor: 'none',
      selectedColorData: false
    };

    this.leaveRoom = this.leaveRoom.bind(this);
    this.changeState = this.changeState.bind(this);
    this.placeCard = this.placeCard.bind(this);
    this.takeCard = this.takeCard.bind(this);
    this.callCardPlace = this.callCardPlace.bind(this);
    this.selectColor = this.selectColor.bind(this);
    this.closeWinnerModal = this.closeWinnerModal.bind(this);
  }

  leaveRoom() {
    const token = session.get('token');
    apiService.leaveHandler(token).then(data => {
      auth.authenticate(data.user, data.token,() => {
        window.location.replace('/rooms/');
      });
    }).catch(error => {
      console.log(error);
    });
  }

  changeState() {
    const newState = (this.props.thisPlayer.status === 1) ? 0 : 1;
    const token = session.get('token');

    apiService.changePlayerState(newState, token).then(data => {
      this.setState({dirty: false});
    }).catch(error => {
      this.setState({dirty: false});
    });
  }

  callCardPlace(color, card, cardIndex, selectedColor, room) {
    const token = session.get('token');
    apiService.placeCard(color, card, cardIndex, selectedColor, room, token).then(data => {
      this.setState({dirty: false});
    }).catch(error => {
      console.log('Could not place card');
    });
  }

  placeCard(color, card, cardIndex) {
    if(this.props.room.gameCurrentPlayer === this.props.thisPlayer.nickname+'-'+this.props.thisPlayer.id) {
      let cardInTheMiddle = this.props.room.gameLastCard;
      let room = this.props.room.name+'-'+this.props.room.id;
      let placeBlock = true;
      let colorPick;

      if(color === 'black') {
        colorPick = true;
        placeBlock = true;
        this.setState({selectedColorData: [color, card, cardIndex, room]});
      }else if(cardInTheMiddle[0] === 'black') {
        if(color === this.props.room.gamePickedColor) {
          placeBlock = false;
        }else {
          console.log('Placing card impossible! x2');
        }
      }else if(color === cardInTheMiddle[0]) {
        placeBlock = false;
      }else if(card === cardInTheMiddle[1]) {
        placeBlock = false;
      }else {
        console.log('Placing card impossible!');
      }


      if(!placeBlock && !colorPick) {
        this.callCardPlace(color, card, cardIndex, 'none', room);
      }else if(placeBlock && colorPick) {
        this.setState({colorPickModalState: true});
      }

    }
  }

  takeCard() {
    if(this.props.room.gameCurrentPlayer === this.props.thisPlayer.nickname+'-'+this.props.thisPlayer.id) {
      let room = this.props.room.name+'-'+this.props.room.id;
      const token = session.get('token');

      apiService.takeCard(room, token).then(data => {
        this.setState({dirty: false});
      }).catch(error => {
        console.log('Could not take card');
      });
    }
  }

  selectColor(color) {
    let colorData = this.state.selectedColorData;

    this.setState({colorPickModalState: false, selectedColorData: false});
    this.callCardPlace(colorData[0], colorData[1], colorData[2], color, colorData[3]);
  }

  closeWinnerModal() {
    this.props.winnerModalClose();
  }

  render() {

    let readyButton = (this.props.thisPlayer.status === 1) ? 'Ready' : 'Not ready';
    let readyButtonColor = (this.props.thisPlayer.status === 1) ? 'green' : 'red';

    let playerList = (this.props.players) ? Object.keys(this.props.players).map((item, i) => <Player key={this.props.players[item].id} status={this.props.players[item].status} id={this.props.players[item].id} nickname={this.props.players[item].nickname} cards={this.props.players[item].cards} currentPlayer={this.props.room.gameCurrentPlayer} thisPlayer={this.props.thisPlayer} />) : null;

    let playerCards = (this.props.thisPlayer.cards) ? Object.keys(this.props.thisPlayer.cards).map((item, i) => <li key={item} className={`card ${this.props.thisPlayer.cards[item][0]} ${this.props.thisPlayer.cards[item][1]} `} onClick={() => this.placeCard(this.props.thisPlayer.cards[item][0], this.props.thisPlayer.cards[item][1], i)} ></li>) : null;

    let pickedColorInfo = (this.props.room.gamePickedColor) ? <Label color={this.props.room.gamePickedColor}>Picked color: {this.props.room.gamePickedColor}</Label> : null;

    return (
      <Grid stackable columns={2}>
        <Grid.Row>
          <Grid.Column width={11}>

            <Grid columns={2}>
              <Grid.Row>
                <Grid.Column width={8}>
                  <div className='take-card card small hidden' onClick={() => this.takeCard()}></div>
                </Grid.Column>
                <Grid.Column width={8}>
                  <div className={`middle-card card small ${this.props.room.gameLastCard[0]} ${this.props.room.gameLastCard[1]} `}></div>
                  {pickedColorInfo}
                </Grid.Column>
              </Grid.Row>
            </Grid>

            <ul className='player-cards'>
              {playerCards}
            </ul>

          </Grid.Column>
          <Grid.Column width={5}>

          <Header as='h3' attached='top'>Options</Header>
          <Segment attached>
            <List divided verticalAlign='middle'>
              <List.Item>
                <List.Content>
                  <Button color='blue' fluid onClick={() => this.leaveRoom()}>Leave room</Button>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Content>
                  <Button color={readyButtonColor} fluid onClick={() => this.changeState()}>{readyButton}</Button>
                </List.Content>
              </List.Item>
            </List>
          </Segment>

          <Header as='h3' attached='top'>Player list</Header>
          <Segment attached>
            <List divided verticalAlign='middle'>
              {playerList}
            </List>
          </Segment>

          </Grid.Column>
        </Grid.Row>

        <PickColorModal
          modalOpen={this.state.colorPickModalState}
          actionHandler={this.selectColor}
        />

        <WinnerModal
          modalOpen={this.props.winnerModalState}
          actionHandler={this.closeWinnerModal}
          winnerName={this.props.room.gameWinner[0]}
        />
      </Grid>
    );
  }

}

export default Game;
