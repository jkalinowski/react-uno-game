import React, { Component } from 'react';

import {Modal, Header, Button, Icon} from 'semantic-ui-react';

class WinnerModal extends Component {

  constructor(props) {
    super(props);

    this.actionAndReset = this.actionAndReset.bind(this);
  }

  actionAndReset(color) {
    this.props.actionHandler(color);
  }

  render() {

    return (
      <Modal open={this.props.modalOpen} basic size='small'>
        <Header icon='archive' content='Game ended!' />
        <Modal.Content>
          <h3>{this.props.winnerName} has won the game!</h3>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.actionAndReset} inverted>
            <Icon name='checkmark'/> Close
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }

}
export default WinnerModal;
