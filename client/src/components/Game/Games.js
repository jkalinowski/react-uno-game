import React, {Component} from 'react';
import {Container, Dimmer, Loader} from 'semantic-ui-react';

import {session} from '../../service/Session';
import {config} from '../../Config';

import apiService from '../../service/apiService';

import auth from '../../service/auth';

import SecureWebsocket from '../../service/SecureWebsocket';

import Game from './Game';

class Games extends Component {
  constructor(props) {
    super(props);

    this.state = {
      room: false,
      players: false,
      thisPlayer: false,
      gameState: 'loading',
      dirty: false,
      winnerState: false
    };

    this.connected = this.connected.bind(this);
    this.loadGameData = this.loadGameData.bind(this);
    this.reloadGameData = this.reloadGameData.bind(this);
    this.closeWinnerModal = this.closeWinnerModal.bind(this);
  }

  componentWillMount() {
    this.connected(this.props.match.params.id);
    this.loadGameData();
  }

  connected(id) {
    apiService.connectHandler(id).then(data => {
      this.setState({dirty: false});
    }).catch(error => {
      console.log(error);
    });
  }

  loadGameData() {
    const token = session.get('token');
    apiService.getRoomById(this.props.match.params.id, token).then(data => {
      auth.authenticate(auth.user, data.token,() => {
        if(data.room.gameWinner) {
          this.setState({winnerState: true});
        }

        console.log(data.room);
        console.log(data.players);

        Object.keys(data.players).map((item, i) => {
          if(data.players[item].id === auth.user.id) {
            let thisPlayer = data.players[item];

            this.setState({ room: data.room, players: data.players, thisPlayer: thisPlayer, gameState: 'loaded' });
          }
        });
      });
    }).catch(error => {
      this.setState({ rooms: false, players: false, thisPlayer: false, gameState: 'loading' });

      //if there is a error - try to refresh the page!
      window.location.reload();
    });
  }

  reloadGameData() {
    console.log('reloading game data');
    this.setState({dirty: false});
    this.loadGameData();
  }

  closeWinnerModal() {
    this.setState({winnerState: false});
  }

  render() {

    switch (this.state.gameState) {
      case 'loading':
        return (
          <Dimmer active>
            <Loader />
          </Dimmer>
        );
      case 'loaded':
        return (
          <Container>
            <Game room={this.state.room} winnerModalState={this.state.winnerState} winnerModalClose={() => this.closeWinnerModal()} players={this.state.players} thisPlayer={this.state.thisPlayer} user={auth} />

            <SecureWebsocket
              url={config.apiServer.socketUrl}
              onMessage={() => this.reloadGameData()}
              reconnect={true}
            />
          </Container>
        );
      default:
        return (
          <Dimmer active>
            <Loader />
          </Dimmer>
        );
    }

  }

}

export default Games;
