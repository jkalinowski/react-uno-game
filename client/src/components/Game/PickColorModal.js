import React, { Component } from 'react';

import {Modal, Header, Button} from 'semantic-ui-react';

class PickColorModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedColor: false
    };

    this.actionAndReset = this.actionAndReset.bind(this);
  }

  actionAndReset(color) {
    this.props.actionHandler(color);
  }

  render() {

    return (
      <Modal open={this.props.modalOpen} basic size='small'>
        <Header icon='archive' content='Pick color' />
        <Modal.Content>
          <Button color='red' inverted onClick={() => this.actionAndReset('red')}>Red</Button>
          <Button color='green' inverted onClick={() => this.actionAndReset('green')}>Green</Button>
          <Button color='blue' inverted onClick={() => this.actionAndReset('blue')}>Blue</Button>
          <Button color='yellow' inverted onClick={() => this.actionAndReset('yellow')}>Yellow</Button>
        </Modal.Content>
      </Modal>
    );
  }

}
export default PickColorModal;
