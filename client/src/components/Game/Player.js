import React, { Component } from 'react';

import { List, Label } from 'semantic-ui-react'

class Player extends Component {

  render() {
    let playerLabel = (this.props.currentPlayer === this.props.nickname+'-'+this.props.id) ? <Label color='green'>yes</Label> : null;
    let isUnoLabel = (this.props.cards.length === 1) ? <Label color='yellow'>UNO</Label> : null;

    let playerCards = (this.props.cards && this.props.thisPlayer.nickname+'-'+this.props.thisPlayer.id !== this.props.nickname+'-'+this.props.id) ? Object.keys(this.props.cards).map((item, i) => <li key={item} className="card ultra-small hidden"></li>) : null;

    return (
      <List.Item>
        <List.Content floated='right'>
          <Label color={(this.props.status === 1) ? 'green' : 'red'}>{(this.props.status === 1) ? 'Ready' : 'Not ready'}</Label>
        </List.Content>
        <List.Content>
          <div className='player'>
            <h4>{this.props.nickname} {playerLabel} {isUnoLabel}</h4>
            <ul>
              {playerCards}
            </ul>
          </div>
        </List.Content>
      </List.Item>
    );
  }
}

export default Player;
