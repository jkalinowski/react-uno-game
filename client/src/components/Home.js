import React, {Component} from 'react'
import {BrowserRouter, Route} from 'react-router-dom';

import PrivateRoute from '../service/PrivateRoute';

import Login from './Login/Login';
import Rooms from './Rooms/Rooms';
import Games from './Game/Games';

class Home extends Component {

  render() {

    return (
      <BrowserRouter>
        <Route exact path="/" component={Login} />

        <PrivateRoute path="/rooms" component={Rooms} />

        <PrivateRoute path="/games/:id" component={Games} />
      </BrowserRouter>
    );
  }

}

export default Home;
