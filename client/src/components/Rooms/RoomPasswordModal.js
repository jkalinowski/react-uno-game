import React, { Component } from 'react';

import {Button, Icon, Modal, Form} from 'semantic-ui-react';

class RoomPasswordModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      roomPassword: ''
    };

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.closeAndReset = this.closeAndReset.bind(this);
    this.actionAndReset = this.actionAndReset.bind(this);
  }

  inputChangeHandler(evt, {name, value}) {
    this.setState({[name]: value});
  }

  closeAndReset() {
    this.setState({roomPassword: ''});
    this.props.closeHandler();
  }

  actionAndReset() {
    this.props.actionHandler(this.state.roomPassword);
    this.setState({roomPassword: ''});
  }

  render() {

    return (
      <Modal
        open={this.props.modalOpen}
        onClose={this.closeAndReset}
        size='small'
      >
        <Modal.Header>Enter room password</Modal.Header>
        <Modal.Content>
          <Form >
            <Form.Field>
              <label>Password</label>
              <Form.Group widths="equal">
                <Form.Input
                  focus
                  name='roomPassword'
                  placeholder='Room password'
                  value={this.state.roomPassword}
                  onChange={this.inputChangeHandler}
                />
              </Form.Group>
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.closeAndReset}>
            <Icon name='cancel' /> Cancel
          </Button>
          <Button color='green' inverted onClick={this.actionAndReset}>
            <Icon name='checkmark' /> Connect to room
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }

}
export default RoomPasswordModal;
