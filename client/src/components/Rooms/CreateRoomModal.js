import React, { Component } from 'react';

import {Button, Icon, Modal, Form} from 'semantic-ui-react';


class CreateRoomModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      roomName: '',
      roomType: '',
      roomPassword: ''
    };

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.closeAndReset = this.closeAndReset.bind(this);
    this.actionAndReset = this.actionAndReset.bind(this);

  }

  inputChangeHandler(evt, {name, value}) {
    this.setState({[name]: value});
  }

  closeAndReset() {
    this.setState({roomName: '', roomType: '', roomPassword: ''});
    this.props.closeHandler();
  }

  actionAndReset() {
    this.props.actionHandler(this.state.roomName, this.state.roomType, this.state.roomPassword);
    this.setState({roomName: '', roomType: '', roomPassword: ''});
  }

  render() {
    let passwordInput = (this.state.roomType === 'private') ?
    <Form.Input
      name='roomPassword'
      placeholder='Enter room password'
      value={this.state.roomPassword}
      onChange={this.inputChangeHandler}
    /> : null;

    return (
      <Modal
        open={this.props.modalOpen}
        onClose={this.closeAndReset}
        size='small'
      >
        <Modal.Header>Create Room</Modal.Header>
        <Modal.Content>
          <Form >
            <Form.Field>
              <label>Name</label>
              <Form.Group widths="equal">
                <Form.Input
                  focus
                  name='roomName'
                  placeholder='Enter a room name'
                  value={this.state.roomName}
                  onChange={this.inputChangeHandler}
                />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Radio
                  label='Private'
                  name='roomType'
                  value='private'
                  checked={this.state.roomType === 'private'}
                  onChange={this.inputChangeHandler}
                />
                <Form.Radio
                  label='Public'
                  name='roomType'
                  value='public'
                  checked={this.state.roomType === 'public'}
                  onChange={this.inputChangeHandler}
                />
              </Form.Group>
              <Form.Group widths='equal'>
                {passwordInput}
              </Form.Group>
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.closeAndReset}>
            <Icon name='cancel' /> Cancel
          </Button>
          <Button color='green' inverted onClick={this.actionAndReset}>
            <Icon name='checkmark' /> Create new room
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }

}
export default CreateRoomModal;
