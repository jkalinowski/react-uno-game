import React, { Component } from 'react';

import { List, Button, Icon, Label } from 'semantic-ui-react'

import auth from "../../service/auth";

class Room extends Component {

  render() {
    let deleteButton = (auth.user.id === this.props.ownerId) ? <Button basic color='red'>Delete</Button> : null;

    return (
      <List.Item key={this.props.id}>
        <List.Content floated='right'>
          <Button positive onClick={() => this.props.connectHandler(this.props.id)}>Connect</Button>

          {deleteButton}
        </List.Content>

        <List.Content>
          <List.Header as='h3'>{this.props.name}</List.Header>
          <Label color='blue'>
            <Icon name='user' /> Owner: {this.props.owner}
          </Label>
          <Label>
            <Icon name={(this.props.type === 'private') ? 'lock' : 'lock open'} /> Type: {this.props.type}
          </Label>
        </List.Content>
      </List.Item>
    );
  }
}

export default Room;
