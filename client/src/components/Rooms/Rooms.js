import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';

import {Container, List, Header, Segment, Button} from 'semantic-ui-react';
import {session} from '../../service/Session';
import {config} from '../../Config';

import apiService from '../../service/apiService';

import SecureWebsocket from '../../service/SecureWebsocket';

import Room from './Room.js';
import CreateRoomModal from './CreateRoomModal.js';
import RoomPasswordModal from './RoomPasswordModal.js';

class Rooms extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: false,
      rooms: false,
      createModalState: false,
      passwordModalState: false,
      dirty: false,
      redirectToGame: false,
      connectingToGame: false
    }

    this.getRoomsData = this.getRoomsData.bind(this);
    this.changeCreateModalState = this.changeCreateModalState.bind(this);
    this.changePasswordModalState = this.changePasswordModalState.bind(this);
    this.reloadRoomsList = this.reloadRoomsList.bind(this);
    this.createRoom = this.createRoom.bind(this);
    this.verifyRoomPassword = this.verifyRoomPassword.bind(this);
  }

  componentWillMount() {
    this.getRoomsData();
  }

  getRoomsData() {
    const token = session.get('token');
    apiService.getRoomsData(token).then(data => {
      this.setState({ user: data.user, rooms: data.rooms });
    }).catch(error => {
      this.setState({ user: false, rooms: false });
    });
  }

  changeCreateModalState(state) {
    this.setState({createModalState: state});
  }

  changePasswordModalState(state) {
    this.setState({passwordModalState: state});
  }

  reloadRoomsList() {
    this.setState({dirty: false});
    this.getRoomsData();
  }

  createRoom(roomName, roomType, roomPassword) {
    if(roomName !== '') {
      let room = {
        name: roomName,
        type: roomType,
        password: roomPassword
      }

      apiService.createNewRoom(this.state.user, room).then(() => {
        this.changeCreateModalState(false);
        this.getRoomsData();
      });
    }
  }

  connectToRoom(id) {
    if(id) {
      Object.keys(this.state.rooms).map((item, i) => {
        let roomEntry = this.state.rooms[item];
        if(roomEntry.id === id) {

          this.setState({connectingToGame: roomEntry});

          if(roomEntry.type === 'private') {
            //if room isn't public, ask for password
            this.setState({passwordModalState: true});
          }else {
            //just connect to the game
            if(!roomEntry.gameState) {
              this.setState({redirectToGame: roomEntry.id});
            }else {
              console.log('game in this room already started!');
            }
          }
        }
        return true;
      });

      return false;
    }
  }

  verifyRoomPassword(password) {
    let roomData = this.state.connectingToGame;

    if(roomData.type === 'private') {
      //verify room password
      if(roomData.password === password) {
        if(!roomData.gameState) {
          this.setState({redirectToGame: roomData.id});
          console.log('connected to private room. Password is correct!');
        }else {
          console.log('game in this room already started!');
        }
      }else {
        console.log('password is not correct!');
      }
    }
  }

  render() {

    let rooms = (this.state.rooms) ? Object.keys(this.state.rooms).map((item, i) => <Room key={i} name={this.state.rooms[item].name} owner={this.state.rooms[item].owner.name} ownerId={this.state.rooms[item].owner.id} type={this.state.rooms[item].type} connectHandler={() => this.connectToRoom(this.state.rooms[item].id)}/>) : null;

    let redirectToGame = (this.state.redirectToGame) ? <Redirect to={'/games/'+this.state.redirectToGame} push={true} /> : null;

    return (
      <Container>
        <Segment clearing>
          <Header floated='right'>
            <Button primary onClick={() => this.changeCreateModalState(true)}>Create room</Button>
          </Header>
          <Header as='h2' floated='left'>
            Hello: {this.state.user.nickname}
          </Header>
        </Segment>

        <Segment>
          <List divided relaxed>
            {rooms}
          </List>
        </Segment>


        <CreateRoomModal
          modalOpen={this.state.createModalState}
          actionHandler={this.createRoom}
          closeHandler={() => this.changeCreateModalState(false)}
        />

        <RoomPasswordModal
          modalOpen={this.state.passwordModalState}
          actionHandler={this.verifyRoomPassword}
          closeHandler={() => this.changePasswordModalState(false)}
        />

        <SecureWebsocket
          url={config.apiServer.socketUrl}
          onMessage={() => this.reloadRoomsList()}
          reconnect={true}
        />

        {redirectToGame}
      </Container>
    );
  }

}

export default Rooms;
