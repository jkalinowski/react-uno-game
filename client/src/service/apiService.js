import request from './request';

function get(url) {
  return request({
    url: url,
    method: 'GET'
  });
}

function checkToken() {
  return request({
    url: '/checkToken',
    method: 'POST'
  });
}

function login(user) {
  return request({
    url: '/login',
    method: 'POST',
    data: {
      user:user
    }
  });
}

function getRoomsData() {
  return request({
    url: '/getRoomsData',
    method: 'POST'
  });
}

function createNewRoom(user, room) {
  return request({
    url: '/createRoom',
    method: 'POST',
    data: {
      user: user,
      room: room
    }
  });
}

function getRoomById(id, user) {
  return request({
    url: '/getRoomById',
    method: 'POST',
    data: {
      roomId: id,
      user: user
    }
  });
}

function changePlayerState(newState) {
  return request({
    url: '/changePlayerState',
    method: 'POST',
    data: {
      newState: newState
    }
  });
}

function connectHandler(id) {
  return request({
    url: '/connectHandler',
    method: 'POST',
    data: {
      id: id
    }
  });
}

function leaveHandler(user) {
  return request({
    url: '/leaveHandler',
    method: 'POST'
  });
}

function placeCard(color, card, cardIndex, pickedColor, room, token) {
  return request({
    url: '/placeCard',
    method: 'POST',
    data: {
      color: color,
      card: card,
      cardIndex: cardIndex,
      pickedColor: pickedColor,
      room: room
    }
  });
}

function takeCard(room, token) {
  return request({
    url: '/takeCard',
    method: 'POST',
    data: {
      room: room
    }
  });
}

const apiService = {

  get, checkToken, login, getRoomsData, createNewRoom, getRoomById, changePlayerState, connectHandler, leaveHandler, placeCard, takeCard

};

export default apiService;
